@extends('layouts.mainframe')

@section('content')
@push('scripts')
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip(); //Tooltip on icons top

            $('.popoverOption').each(function () {
                var $this = $(this);
                $this.popover({
                    trigger: 'hover',
                    placement: 'left',
                    container: $this,
                    html: true,

                });
            });
        });
    </script>
@endpush
<div class="row">
    <div class="col-lg-3">
        <!-- Members online -->
        <div class="panel bg-indigo-300">
            <div class="panel-body">
                <h3 class="no-margin">
                    @foreach($taskCompletedThisMonth as $thisMonth)
                    {{$thisMonth->total}}
                    @endforeach
                </h3>
                {{ __('Tasks completed this month') }}
                <div class="text-size-small">
                    <a href="{{route('tasks.index')}}" class="text-muted ">{{ __('All Tasks') }} 
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="container-fluid">
                <div id="members-online"></div>
            </div>
        </div>
        <!-- /members online -->
    </div>
    <div class="col-lg-3">
        <!-- Members online -->
        <div class="panel bg-pink-400">
            <div class="panel-body">
                <h3 class="no-margin">
                    @foreach($leadCompletedThisMonth as $thisMonth)
                    {{$thisMonth->total}}
                    @endforeach
                </h3>
                {{ __('Leads completed this month') }}
                <div class="text-size-small">
                    <a href="{{route('leads.index')}}" class="text-muted ">{{ __('All Leads') }} 
                    </a>
                </div>
            </div>

            <div class="container-fluid">
                <div id="members-online"></div>
            </div>
        </div>
        <!-- /members online -->

    </div>
    <div class="col-lg-3">
        <!-- Members online -->
        <div class="panel bg-slate">
            <div class="panel-body">
                <h3 class="no-margin">
                    {{$totalClients}}
                </h3>
                {{ __('All Clients of the system') }} 
                <div class="text-size-small">
                    <a href="{{route('clients.index')}}" class="text-muted ">{{ __('All Clients') }} 
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="container-fluid">
                <div id="members-online"></div>
            </div>
        </div>
        <!-- /members online -->

    </div>
    <div class="col-lg-3">
        <!-- Members online -->
        <div class="panel bg-green-300">
            <div class="panel-body">
                <h3 class="no-margin">
                    @foreach($totalTimeSpent[0] as $sum => $value)

                    {{$value}}
                    @endforeach
                    @if($value == "")
                    0
                    @endif
                </h3>
                {{ __('Total hours registered') }}
                <div class="text-size-small">
                    <a href="#" class="text-muted ">{{ __('More info') }} 
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="container-fluid">
                <div id="members-online"></div>
            </div>
        </div>
        <!-- /members online -->

    </div>
</div>

<?php $createdTaskEachMonths = array();?>
@foreach($createdTasksMonthly as $key => $task)
    <?php $createdTaskEachMonths[$key]['date'] = date('m/d/y', strTotime($task->created_at)) ?>
    <?php $createdTaskEachMonths[$key]['alpha'] = $task->day ?>
@endforeach


<?php $completedTaskEachMonths = array();?>
@foreach($completedTasksMonthly as $key => $task)
    <?php $completedTaskEachMonths[$key]['date'] = date('m/d/y', strTotime($task->updated_at)) ?>
    <?php $completedTaskEachMonths[$key]['alpha'] = $task->day ?>
@endforeach

<?php $completedLeadEachMonths = array(); $leadsCompleted = array();?>
@foreach($completedLeadsMonthly as $leads)
    <?php $completedLeadEachMonths[] = date('F', strTotime($leads->updated_at)) ?>
    <?php $leadsCompleted[] = $leads->month;?>
@endforeach

<?php $createdLeadEachMonths = array(); $leadCreated = array();?>
@foreach($createdLeadsMonthly as $lead)
    <?php $createdLeadEachMonths[] = date('F', strTotime($lead->created_at)) ?>
    <?php $leadCreated[] = $lead->month;?>
@endforeach


    @include('partials.dashboardone')

@endsection
