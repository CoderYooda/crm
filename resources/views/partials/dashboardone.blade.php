<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">{{ __('Today statistic') }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                <div class="heading-elements">
                    <span class="heading-text"><i class="icon-history text-warning position-left"></i> TODO(today)</span>
                </div>
            </div>

            <!-- Numbers -->
            <div class="container-fluid">
                <div class="row text-center">
                    <div class="col-md-3">
                        <div class="content-group">
                            <h6 class="text-semibold no-margin"><i class="icon-task position-left text-slate"></i> {{$completedTasksToday}}</h6>
                            <span class="text-muted text-size-small">{{ __('Tasks completed today') }}</span>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="content-group">
                            <h6 class="text-semibold no-margin"><i class="icon-inbox position-left text-slate"></i> {{$createdTasksToday}}</h6>
                            <span class="text-muted text-size-small">{{ __('Tasks created today') }}</span>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="content-group">
                            <h6 class="text-semibold no-margin"><i class="icon-stack3 position-left text-slate"></i> {{$completedLeadsToday}}</h6>
                            <span class="text-muted text-size-small">{{ __('Leads completed today') }}</span>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="content-group">
                            <h6 class="text-semibold no-margin"><i class="icon-stack2 position-left text-slate"></i> {{$createdLeadsToday}}</h6>
                            <span class="text-muted text-size-small">{{ __('Leads created today') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /numbers -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <!-- Today's revenue -->
        <div class="panel bg-blue-400">
            <div class="panel-body">
                <h3 class="no-margin">{{ __('Tasks created in this months') }}</h3>
                {{ __('All of users') }}
            </div>

            <div id="tasks-revenue"></div>
        </div>
        <!-- /today's revenue -->
    </div>
    <div class="col-lg-6">
        <!-- Today's revenue -->
        <div class="panel bg-blue-400">
            <div class="panel-body">
                <h3 class="no-margin">{{ __('Tasks complited in this months') }}</h3>
                {{ __('All of users') }}
            </div>

            <div id="tasks-complited"></div>
        </div>
        <!-- /today's revenue -->
    </div>
</div>
<script>
    // Add data set
    var createdTasks = <?php echo json_encode($createdTaskEachMonths) ?>;
    var taskCreatedArray = infograpficGenerateMonthly(createdTasks);
    var TCtextdata = [];
    TCtextdata.tooltiptext = "{{ __('Tasks setted:') }}";
    infoInMonths('#tasks-revenue', 60, taskCreatedArray, TCtextdata);
    
    var complitedTasks = <?php echo json_encode($completedTaskEachMonths) ?>;
    var taskComplitedArray = infograpficGenerateMonthly(complitedTasks);
    var CTtextdata = [];
    CTtextdata.tooltiptext = "{{ __('Tasks complited:') }}";
    infoInMonths('#tasks-complited', 60, taskComplitedArray, CTtextdata);

</script>
<div class="panel text-center">
    <div class="panel-body">
<div class="row">
    <div class="col-lg-6">
        <div class="content-group-sm svg-center position-relative" id="totalPercentageTasks"></div> 
    </div>
    <div class="col-lg-6">
        <div class="content-group-sm svg-center position-relative" id="totalPercentageLeads"></div> 
    </div>
</div>
    </div>
</div>
@push('scripts')
<script>
progressCounter('#totalPercentageTasks', 38, 2, "#689F38", {{(number_format($totalPercentageTasks, 0))/100}}, "icon-watch text-pink-400", '{{ __('All Tasks') }}', '{{$allCompletedTasks}} / {{$alltasks}} {{ __('Complited') }}');
progressCounter('#totalPercentageLeads', 38, 2, "#F57C00", {{(number_format($totalPercentageLeads, 0))/100}}, "icon-watch text-pink-400", '{{ __('All Leads') }}', '{{$allCompletedLeads}} / {{$allleads}} {{ __('Complited') }}');
</script>
@endpush

<div class="row">
    <div class="col-md-12">

        <!-- List with text -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{ __('Users') }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            </div>
            <ul class="media-list media-list-linked">
                @foreach($users as $user)
                <li class="media">
                    <a href="{{route('users.show', $user->id)}}" class="media-link">
                        <div class="media-left"><img 
                                 @if($user->image_path != "")
                                 src="images/{{$companyname}}/{{$user->image_path}}"
                                 @else
                                 src="images/default_avatar.jpg"
                                    @endif
                                 class="img-circle" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">{{$user->name}}</div>
                            <span class="text-muted">Development</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            @if($user->isOnline())
                            <span class="label label-success">online</span>
                            @else
                            <span class="label label-danger">offline</span>
                            @endif
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        <!-- /list with text -->
    </div>
</div>
</div>



    
