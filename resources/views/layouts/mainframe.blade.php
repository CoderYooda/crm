<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AVA CRM</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/css/components.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/css/colors.css') }}" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/core/app.js') }}"></script>
        <!-- /core JS files -->

        <!-- Theme JS files-->
        
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/ui/moment/moment_locales.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>

        
<!--        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/dashboard_boxed.js') }}"></script>-->
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/dashboard.js') }}"></script>
        <!--/theme JS files -->



    </head>

    <body class="layout-boxed navbar-bottom">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-boxed">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a>

                    <ul class="nav navbar-nav visible-xs-block">
                        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                        <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>
                </div>

                <div class="navbar-collapse collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav navbar-right">
                        <!--Оповещения-->
                        <?php $notifications = auth()->user()->unreadNotifications; ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-bell2"></i>
                                <span class="visible-xs-inline-block position-right">{{ __('Notifications') }}</span>
                                <span class="badge bg-warning-400 border-slate-800 border-xlg">{{ $notifications->count() }}</span>
                            </a>

                            <div class="dropdown-menu dropdown-content">
                                <ul class="media-list dropdown-content-body width-350">
                                    @if(count($notifications))
                                    @foreach($notifications as $notification)
                                    <li class="media">
                                        <div class="media-left">
                                            <a href="{{ route('notification.read', ['id' => $notification->id])  }}" onClick="postRead({{ $notification->id }})" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-bubble-notification"></i></a>
                                        </div>

                                        <div class="media-body">
                                            {{ $notification->data['message']}}
                                            <div class="media-annotation">4 minutes ago</div>
                                        </div>
                                    </li>
                                    @endforeach 
                                    @else
                                    <div class="text-center">
                                    {{ __('No notifications') }}
                                    </div>
                                    @endif
                                </ul>

                                @push('scripts')
                                <script>      
                                    id = {};
                                    function postRead(id) {
                                        $(this).closest(".media").remove();
                                        $.ajax({
                                            type: 'post',
                                            url: '{{url('/notifications/markread')}}',
                                            data: {
                                                id: id,
                                            },
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            }
                                        });

                                    }

                                </script>
                                @endpush
                                
                                <div class="dropdown-content-footer">
                                    <a href="{{route('leads.index')}}" data-popup="tooltip" title="{{ __('All notifications') }}"><i class="icon-menu display-block"></i></a>
                                </div>
                            </div>
                        </li>
                        <!--/Оповещения-->
                        <li class="dropdown dropdown-user">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <img src="assets/images/placeholder.jpg" alt="">
                                <span>{{Auth::user()->name}}</span>
                                <i class="caret"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{route('users.show', \Auth::id())}}"><i class="icon-profile"></i> {{ __('Profile') }}</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ url('/logout') }}"><i class="icon-switch2"></i> {{ __('Sign Out') }}</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page header -->
        <div class="page-header">
            <div class="breadcrumb-line">
                <div class="breadcrumb-boxed">
                    <ul class="breadcrumb-elements">
                        @if(Entrust::hasRole('administrator'))
                        <li class="dropdown">    
                            <a href="#settings" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-gear position-left"></i>
                                {{ __('Settings') }}
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{ route('settings.index')}}"><i class="icon-user-lock"></i> {{ __('Overall Settings') }}</a></li>
                                <li><a href="{{ route('roles.index')}}"><i class="icon-statistics"></i> {{ __('Role Management') }}</a></li>
                                <li><a href="{{ route('integrations.index')}}"><i class="icon-accessibility"></i> {{ __('Integrations') }}</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="page-header-content">
                <div class="page-title">
                    <h4><span class="text-semibold">@yield('heading')</span></h4>
                </div>

                <div class="heading-elements">
                    <div class="heading-btn-group">
                        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main sidebar-default">
                    <div class="sidebar-content">

                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">
                                    <li><a href="{{route('dashboard', \Auth::id())}}"><i class="icon-home4"></i> <span>{{ __('Dashboard') }}</span></a></li>
                                    <li><a href="{{route('users.show', \Auth::id())}}"><i class="icon-profile"></i> <span>{{ __('Profile') }}</span></a></li>
                                    <li class="navigation-header"><span>{{ __('Clients menus') }}</span> <i class="icon-menu" title="Main pages"></i></li>
                                    <li>
                                        <a href="#clients" class="has-ul"><i class="icon-user-check"></i> <span>{{ __('Clients') }}</span></a>
                                        <ul class="hidden-ul">
                                            <li><a href="{{ route('clients.index')}}">{{ __('All Clients') }}</a></li>
                                            @if(Entrust::can('client-create'))
                                            <li><a href="{{ route('clients.create')}}">{{ __('New Client') }}</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#tasks" class="has-ul"><i class="icon-task"></i> <span>{{ __('Tasks') }}</span></a>
                                        <ul class="hidden-ul">
                                            <li><a href="{{ route('tasks.index')}}">{{ __('All Tasks') }}</a></li>
                                            @if(Entrust::can('task-create'))
                                            <li><a href="{{ route('tasks.create')}}">{{ __('New Task') }}</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#user" class="has-ul"><i class="icon-users4"></i> <span>{{ __('Users') }}</span></a>
                                        <ul class="hidden-ul">
                                            <li><a href="{{ route('users.index')}}">{{ __('Users All') }}</a></li>
                                            @if(Entrust::can('user-create'))
                                            <li><a href="{{ route('users.create')}}">{{ __('New User') }}</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#leads" class="has-ul"><i class="icon-user-tie"></i> <span>{{ __('Leads') }}</span></a>
                                        <ul class="hidden-ul">
                                            <li><a href="{{ route('leads.index')}}">{{ __('All Leads') }}</a></li>
                                            @if(Entrust::can('lead-create'))
                                            <li><a href="{{ route('leads.create')}}">{{ __('New Lead') }}</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#departments" class="has-ul"><i class="icon-archive"></i> <span>{{ __('Departments') }}</span></a>
                                        <ul class="hidden-ul">
                                            <li><a href="{{ route('departments.index')}}">{{ __('All Departments') }}</a></li>
                                            @if(Entrust::hasRole('administrator'))
                                            <li><a href="{{ route('departments.create')}}">{{ __('New Department') }}</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                    @if(Entrust::hasRole('administrator'))
                                    <li class="navigation-header"><span>{{ __('Admin menus') }}</span> <i class="icon-menu" title="Main pages"></i></li>
                                    <li>
                                        <a href="#settings" class="has-ul"><i class="icon-gear"></i> <span>{{ __('Settings') }}</span></a>
                                        <ul class="hidden-ul">
                                            <li><a href="{{ route('settings.index')}}">{{ __('Overall Settings') }}</a></li>
                                            <li><a href="{{ route('roles.index')}}">{{ __('Role Management') }}</a></li>
                                            <li><a href="{{ route('integrations.index')}}">{{ __('Integrations') }}</a></li>
                                        </ul>
                                    </li>
                                    @endif
                                    <li class="navigation-header"><span>{{ __('Account menus') }}</span> <i class="icon-menu" title="Main pages"></i></li>
                                    <li><a href="{{ url('/logout') }}"><i class="icon-switch2"></i> <span>{{ __('Sign Out') }}</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->

                    </div>
                </div>
                <!-- /main sidebar -->

                <!-- Всплывающие оповещения -->
                @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>

                @endif
                @if(Session::has('flash_message_warning'))
                <message message="{{ Session::get('flash_message_warning') }}" type="warning"></message>
                @endif
                @if(Session::has('flash_message'))
                <message message="{{ Session::get('flash_message') }}" type="success"></message>
                @endif
                <!-- /Всплывающие оповещения -->
                
                <!-- Контент страницы -->
                <div class="content-wrapper">
                    @yield('content')
                </div>
                <!-- /Контент страницы -->


            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


    <!-- Footer -->
    <div class="navbar navbar-default navbar-fixed-bottom footer">
        <div class="navbar-boxed">
            <ul class="nav navbar-nav visible-xs-block">
                <li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
            </ul>

            <div class="navbar-collapse collapse" id="footer">
                <div class="navbar-text">
                    &copy; 2018. <a href="#" class="navbar-link">AVA CRM Web App</a> by <a href="#" class="navbar-link" target="_blank">CoderYooda</a>
                </div>

                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="#">About</a></li>
                        <li><a href="#">Terms</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /footer -->
</body>
<!-- Стек скриптов из шаблона -->
@stack('scripts')
<!-- /Стек скриптов из шаблона -->
</html>
